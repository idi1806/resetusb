#include <usb.h>
#include <stdio.h>

int main(void)
{
   usb_init();
   usb_find_busses();
   usb_find_devices();
   
   /* iterate and reset */
   int                  cnt        = 0;
   struct usb_bus       *usbBusPtr = usb_get_busses();
   while (usbBusPtr)
   {
      struct usb_device *usbDevPtr = usbBusPtr->devices;
      while (usbDevPtr)
      {
         char           name[1024];
         usb_dev_handle *devHandle = usb_open(usbDevPtr);
         usb_get_string_simple(devHandle, 2, name, sizeof(name)-1);
         if (devHandle) printf("[%d]: reset %p %d (%s)\n", cnt++, usbDevPtr, usb_reset(devHandle), name);
         else           printf("[%d]: usb_open %p (%s) failed\n", cnt++, usbDevPtr, name);
         
         /* next device */
         usbDevPtr = usbDevPtr->next;
      }
      /* next bus */
      usbBusPtr    = usbBusPtr->next;
   }
}
